﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AstroidCount : MonoBehaviour {
	GameObject gameController;

	void Start(){
		gameController = GameObject.Find("Game Controller");
	}

	void Update(){
		this.GetComponent<Text>().text = "Destroyed: " + (int)gameController.GetComponent<Controller>().astroidsDestroyed;
	}
}
