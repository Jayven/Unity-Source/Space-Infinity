﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lives : MonoBehaviour {
	GameObject gameController;

	void Start(){
		gameController = GameObject.Find("Game Controller");
	}

	void Update(){
		this.GetComponent<Text>().text = "Lives Used: " + (int)gameController.GetComponent<Controller>().lives;
	}
}
