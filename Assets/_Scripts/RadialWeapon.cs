﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadialWeapon : MonoBehaviour {
	public float cooldown;
	public float range;
	public float speed;
	public GameObject shot;
	public float nextFire = 0;

	public bool fire(){
		if (nextFire < Time.time){
			nextFire = Time.time + cooldown;
			
			for (int i = 0; i < 360; i += 6){
				GameObject newShot = Instantiate(
					shot,
					this.transform.position,
					Quaternion.Euler(new Vector3(0,i,0))
				) as GameObject;
				newShot.GetComponent<Mover>().init(speed, range);
			}

			this.GetComponent<AudioSource>().Play();
			return true;
		}
		else{
			return false;
		}
	}
}
