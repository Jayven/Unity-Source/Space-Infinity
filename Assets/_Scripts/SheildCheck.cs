using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SheildCheck : MonoBehaviour {
	public GameObject controller;
	public float percent;

	void Start(){
		controller = GameObject.Find("Game Controller");
	}
	void Update () {
		if (controller.GetComponent<Controller>().myPlayer){
			percent = 
				1
				-
				(
				controller.GetComponent<Controller>().myPlayer.transform.GetChild(2).gameObject.GetComponent<RadialWeapon>().nextFire
				-
				Time.time
				)
				/
				controller.GetComponent<Controller>().myPlayer.transform.GetChild(2).gameObject.GetComponent<RadialWeapon>().cooldown
			;
			
			GetComponent<Image>().fillAmount = percent;

			GetComponent<Image>().color = percent > .99 ? new Color(.06F,.28F,.22F,.39F) : new Color(.26F,.01F,.01F,.39F);
		}
	}
}
