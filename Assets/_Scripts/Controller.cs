﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour {
	public GameObject Hazard;
	public GameObject Player;
	public GameObject myPlayer;

	public float score;
	public float lives;
	public float astroidsDestroyed;

	public float nextWave;
	public float nextSpawnTime = 1;
	public float respawnDelay;

	void Update(){
		if (Time.time > nextWave){
			SpawnWave(Mathf.FloorToInt(Time.time));
			nextWave = nextWave + 3;
		}
		if (Time.time > nextSpawnTime && nextSpawnTime != 0){
			clearAstroids();
			myPlayer = Instantiate(Player, new Vector3(0,0,0), Quaternion.Euler(new Vector3(0,0,0)));
			nextSpawnTime = 0;
		}
		score = astroidsDestroyed * 10 - Time.time - lives * 100;
	}

	void SpawnWave(int difficulty){
		for (int i = 0; i < Mathf.Sqrt(Mathf.Sqrt(difficulty)); i++){
			Instantiate(Hazard,
				new Vector3(Random.value * 12 - 6, 0, Random.value * 7 + 7),
				Quaternion.Euler(0,0,0)
			);
		}
	}

	public void playerDeath(){
		nextSpawnTime = Time.time + respawnDelay;
		respawnDelay++;
		lives++;
	}

	public void clearAstroids(){
		GameObject [] Astroids = GameObject.FindGameObjectsWithTag("Astroid"); 
		for (int i = 0; i < Astroids.Length; i++){
			Destroy(Astroids[i]);
		}
	}
	
}
