﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	public GameObject shot;
	public GameObject weapon1;
	public GameObject weapon2;
	public GameObject explosion;
	public GameObject player;
	public GameObject creation;

	GameObject gameController;

	void Start(){
		Instantiate(creation, transform.position, transform.rotation);
		gameController = GameObject.Find("Game Controller");
	}
	void Update(){
		if (Input.GetButton("Fire1")){
			weapon1.GetComponent<WeaponScript>().fire();
		}
		if (Input.GetButton("Fire2")){
			weapon2.GetComponent<RadialWeapon>().fire();
		}
	}

	public float max_tilt;
	public float speed;
	public float rotVel;
	public float torqResist;
	public void FixedUpdate(){
		Rigidbody rb = GetComponent<Rigidbody>();
		//Get input
		float moveHorizontal = Input.GetAxis("Horizontal");
		float moveVertical = Input.GetAxis("Vertical");

		//Manage tilt
		if (rb.velocity.x != 0.0f){
			float deltaZ = rb.rotation.eulerAngles.z - rb.velocity.x * rotVel;
			if (deltaZ >= max_tilt && deltaZ <= 360 - max_tilt){
				deltaZ = rb.rotation.eulerAngles.z;
			}
			rb.MoveRotation(
				Quaternion.Euler(
					0,
					0,
					deltaZ
				)
			);
		}
		else{//Reduce tilt because we stopped
							//if >280 then increase to 360, else decrease to 0
			float newZ = rb.rotation.eulerAngles.z > 280 ? rb.rotation.eulerAngles.z + torqResist : rb.rotation.eulerAngles.z - torqResist;
			if (newZ < Mathf.Abs(torqResist)){
				//Prevents a flicker of rot.z -= random small float
				newZ = 0;
			}
			rb.MoveRotation(
				Quaternion.Euler(
					0,
					0,
					newZ
				)
			);
		}

		//Manage velocity
		Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
		rb.velocity = movement * speed;

		//Manage Bounderies
		rb.position = new Vector3(
			Mathf.Clamp(rb.position.x, -6.0f, 6.0f),
			0.0f,
			Mathf.Clamp(rb.position.z, -3.0f, 14.0f)
		);
	}
	//Collisions
	void OnTriggerEnter(Collider otherObject){
		if(otherObject.tag != "Null" && otherObject.tag != "Player"){
			Instantiate(explosion, transform.position, transform.rotation);

			Destroy(otherObject.gameObject);
			Destroy(this.gameObject);

			gameController.GetComponent<Controller>().playerDeath();
		}
		//Instantiate(player, new Vector3(0,0,0), Quaternion.Euler(new Vector3(90,0,0)));
	}
}
