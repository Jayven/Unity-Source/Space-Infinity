﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Astroid : MonoBehaviour {
	public float tumble;
	public float max_speed;

	public GameObject Explosion;

	void OnTriggerEnter(Collider otherObject){
		if (otherObject.tag != "Null"){
			Instantiate(Explosion, transform.position, transform.rotation);
			Destroy(otherObject.gameObject);
			Destroy(this.gameObject);
		}
	}
	void Update(){
		Rigidbody rb = GetComponent<Rigidbody>();
		rb.position = new Vector3(rb.position.x, 0, rb.position.z);
	}
	void Start(){
		Rigidbody rb = GetComponent<Rigidbody>();
		rb.velocity = new Vector3(((float)(Random.value - .5) * max_speed), 0, ((float)(Random.value - .75) * max_speed));
		rb.rotation = Quaternion.Euler(new Vector3(Random.value * 360, Random.value * 360, Random.value * 360));
		rb.angularVelocity = Random.insideUnitSphere * tumble;
	}
}
