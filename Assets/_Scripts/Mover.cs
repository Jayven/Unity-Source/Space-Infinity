﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {
	public float exp_time;
	public GameObject mainController;

	public void init(float p_speed, float p_range){
		mainController = GameObject.Find("Game Controller");
		Rigidbody rb = GetComponent<Rigidbody>();
		rb.velocity = transform.forward * p_speed;

		exp_time = Time.time + p_range;
	}
	void Update(){
		if (Time.time > exp_time){
			Object.Destroy(this.gameObject);
		}
	}
	
	void OnTriggerEnter(Collider otherObject){
		if (otherObject.tag == "Astroid"){
			mainController.GetComponent<Controller>().astroidsDestroyed++;
		}
	}
}
