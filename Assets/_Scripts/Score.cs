﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {
	GameObject gameController;

	void Start(){
		gameController = GameObject.Find("Game Controller");
	}

	void Update(){
		this.GetComponent<Text>().text = "Score: " + (int)gameController.GetComponent<Controller>().score;
	}
}
