﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponScript : MonoBehaviour {
	public float cooldown;
	public float range;
	public float speed;
	public GameObject shot;
	public float nextFire = 0;

	public bool fire(){
		if (nextFire < Time.time){
			nextFire = Time.time + cooldown;

			GameObject newShot = Instantiate(shot, this.transform.position, this.transform.rotation) as GameObject;
			newShot.GetComponent<Mover>().init(speed, range);

			this.GetComponent<AudioSource>().Play();
			return true;
		}
		else{
			return false;
		}
	}
}
